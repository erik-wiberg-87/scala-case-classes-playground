package se.erikwiberg.carshopstats

import java.time.LocalDateTime
import scala.util.Random

object Main {
  def main(args: Array[String]): Unit = {
    
    println { "six sample names" }
    Name.sixSampleNames() foreach println
    
    println { "\nmake cars from default brand list" } 
    Car makeCars 12 foreach println
    println { "\nmake cars from provided brands" }
    val ownModelList = "Kia,Fiat,Jaguar,Mazda".split(",").toList
    (Car makeCars 8) { ownModelList } foreach println
  }
  
}

case class Customer(name: Name, repairAppointments: Set[RepairAppointment])
case class Name(firstName: String, lastName: String)
case class Car(modelName: String, year: Int)
case class RepairAppointment(time: LocalDateTime, car: Car)

object RepairAppointment {
  def makeAppointmentsForCars(numberOfAppointments: Int, cars: List[Car]): List[RepairAppointment] = {
    //TODO: also re-think the modeling of the domain.  
    // Does a customer have appointments, or does an appointment have an associated customer?
    // 
    ???
  }
}

object Car {
  private val sampleModelNames = "Volvo,Saab,BMW,Tesla,Toyota" split "," toList

  def makeCars(numberOfCars: Int)(implicit modelNames: List[String] = sampleModelNames): List[Car] = {
    if(numberOfCars < 1) List()
    var cars: List[Car] = List()
    
    for(i <- 1 to numberOfCars) {
      val year = RandomHelper.randomInt(1995, 2016 + 1)
      val randModelNameIndex = RandomHelper.randomInt(0, modelNames.length)
      val modelName = modelNames(randModelNameIndex)
      val car = Car(modelName,year)
      cars = car::cars
    }
    
    cars.toList
  }
}

object Name {
  def sixSampleNames(): List[Name] = {
      val sirNames = "Sigrud,Knut,Johan,Karl,Erik,Fredrik" split "," toList
      val lastNames = sirNames.map(_.concat("sson"))
      val nameTuples = sirNames zip (lastNames reverse)
      nameTuples map { case (firstName,lastName) => Name(firstName,lastName) }
    }
}

object RandomHelper {
    def randomInt(start:Int, end: Int): Int = {
    start + (Random nextInt(end - start ))
  }
}